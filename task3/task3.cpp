﻿#include <iostream>
#include <vector>

using namespace std;

vector<int> values(20, 4);

void inc(int index) {
	if (index < 0) return ;
	else {
		values.at(index--)++;
		inc(index);
	}
}


int main()
{
	setlocale(LC_ALL, "");
	inc(values.size()-1);
	for (auto tmp : values) {
		cout << tmp << endl;
	}
}

